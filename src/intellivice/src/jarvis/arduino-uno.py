#!/usr/bin/env python

import os, sys ; sys.path.append(os.path.join(os.path.dirname(os.path.dirname(__file__)), 'lib/python'))

#*******************************************************************************

from sparkle.shortcuts import *

mgr = Manager('jarvis')

################################################################################

rrd = mgr.feed('collectd', 'local-rrd', host='localhost', port=25826)

################################################################################

tty = mgr.watch('tty', 'uno', device='/dev/tty'+'AMA0', baud=9600, refresh=2.0)

#rrd.intercept('weather', tty=('uno',))

#*******************************************************************************

@tty.register_hook
def process_alarm(dog, **params):
    print "{alarm}\t%s" % repr(params)

#*******************************************************************************

@tty.register_hook
def process_rfid(dog, block0):
    print "{rfid}\t%s" % block0

#*******************************************************************************

@tty.register_hook
def process_weather(dog, **params):
    for key in params:
        params[key] = float(params[key])

    print "{weather}\t%s" % repr(params)

    dog.report_data('weather', **params)

################################################################################

if __name__=='__main__':
    mgr.asyncloop()
