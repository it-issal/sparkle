#!/usr/bin/python
#-*- coding: utf-8 -*-

import sys ; sys.path.append('/vice/lib/python')

################################################################################

from vice.shortcuts import *

from vice.contrib.AdaFruit.RFID import MFRC522

################################################################################

import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BOARD)
GPIO.setwarnings(False)

try:
    import asyncio
except ImportError:
    # Trollius >= 0.3 was renamed
    import trollius as asyncio

from autobahn.asyncio import wamp, websocket

################################################################################

Relais = {
    'r1': { 'pin': 12, 'label': "Lampe 1", 'state': False },
    'r2': { 'pin': 16, 'label': "Lampe 2", 'state': False },
}

for key in Relais:
    Relais[key]['key'] = key
    
    GPIO.setup(Relais[key]['pin'], GPIO.OUT)

################################################################################

class MyFrontendComponent(wamp.ApplicationSession):
    """
    Application code goes here. This is an example component that calls
    a remote procedure on a WAMP peer, subscribes to a topic to receive
    events, and then stops the world after some events.
    """

    def onConnect(self):
        self.mifare = MFRC522()

        self.join(u"streams.scubadev.com", [u"wampcra"], u"joe")

    def onChallenge(self, challenge):
        if challenge.method == u"wampcra":
            signature = auth.compute_wcs(u"secret2".encode('utf8'),
                                      challenge.extra['challenge'].encode('utf8'))
            return signature.decode('ascii')
        else:
            raise Exception("don't know how to handle authmethod {}".format(challenge.method))

    @asyncio.coroutine
    def onJoin(self, details):
        # REGISTER a procedure for remote calling
        #
        def on_meta():
            return {
                'relais': Relais,
            }

        reg = yield self.register(on_meta, 'jarvis.phenix.meta')
        print("procedure meta() registered: {}".format(reg))

        # REGISTER a procedure for remote calling
        #
        def on_relai(key, state):
            if key in Relais:
                GPIO.output(Relais[key]['pin'], not state)

            print("relai() called with {} and {}".format(key, state))

            return state

        reg = yield self.register(on_relai, 'jarvis.phenix.relai')
        print("procedure relai() registered: {}".format(reg))

        while True:
            for key in ('weather', 'rfid'):
                hnd = getattr(self, 'check_%s' % key, None)

                if callable(hnd):
                    print "\t-> Checking %s :" % key

                    for obj in hnd():
                        yield obj

    ############################################################################

    def onLeave(self, details):
        self.disconnect()

    def onDisconnect(self):
        asyncio.get_event_loop().stop()

    ############################################################################

    def check_weather(self):
        try:
            import Adafruit_DHT

            h, t = Adafruit_DHT.read_retry(Adafruit_DHT.DHT11, 4)
        except Exception as ex:
            print ex

        if h is None:
            h = randrange(0, 100, 0.1, float)

        if t is None:
            t = randrange(20, 40, 0.01, float)

        p = randrange(50, 1000, 1, int)

        print "\t... and the weather is : %f°, %f%%, %fkWh" % (t, h, p)

        yield self.publish('jarvis.phenix.weather', [{
            'temp': {
                'value': t,
                'unit': 'C',
            },
            'humid': {
                'value': h,
                'unit': '%',
            },
            'power': {
                'value': p,
                'unit': 'kWh',
            },
        }])

        #self.factory.loop.call_later(1, self.doLoop)

        self.check_rfid()

    def check_rfid(self):
        # Scan for cards
        (status,TagType) = self.mifare.MFRC522_Request(self.mifare.PICC_REQIDL)

        # If a card is found
        if status == self.mifare.MI_OK:
            print "Card detected"

        # Get the UID of the card
        (status,uid) = self.mifare.MFRC522_Anticoll()

        # If we have the UID, continue
        if status == self.mifare.MI_OK:
            meta = {
                'card': {
                    'uid': uid,
                    'nrw': ' '.join([str(x) for x in uid] or ['']),
                    'tag': TagType,
                },
                'blocks': {},
            }

            # This is the default key for authentication
            key = [0xFF,0xFF,0xFF,0xFF,0xFF,0xFF]

            # Select the scanned tag
            self.mifare.MFRC522_SelectTag(uid)

            # Authenticate
            status = self.mifare.MFRC522_Auth(self.mifare.PICC_AUTHENT1A, 8, key, uid)

            # Check if authenticated
            if status == self.mifare.MI_OK:
                for blockAddr in range(1, 8):
                    recvData = []
                    recvData.append(self.mifare.PICC_READ)
                    recvData.append(blockAddr)
                    pOut = self.mifare.CalulateCRC(recvData)
                    recvData.append(pOut[0])
                    recvData.append(pOut[1])
                    (status, backData, backLen) = self.mifare.MFRC522_ToCard(self.mifare.PCD_TRANSCEIVE, recvData)
                    if not(status == self.mifare.MI_OK):
                      print "Error while reading!"
                    i = 0
                    if len(backData) == 16:
                        meta['blocks'][blockAddr] = backData

                self.mifare.MFRC522_StopCrypto1()

                print "Authentication ok"
            else:
                print "Authentication error"

            meta['auth'] = {
                'challenge': key,
                'result':    (status==self.mifare.MI_OK),
            }

            print meta

            yield self.publish('jarvis.phenix.rfid', [meta])

        self.check_weather()

################################################################################

if __name__ == '__main__':
    print "Preparing the agent ..."
    session_factory = wamp.ApplicationSessionFactory()
    session_factory.session = MyFrontendComponent

    transport_factory = websocket.WampWebSocketClientFactory(session_factory,
                                                             debug=True,
                                                             debug_wamp=False)

    loop = asyncio.get_event_loop()
    coro = loop.create_connection(transport_factory, 'streams.scubadev.com')
    loop.run_until_complete(coro)

    loop.run_forever()

    GPIO.cleanup()
