#!/usr/bin/python
#-*- coding: utf-8 -*-

import sys ; sys.path.append('/vice/lib/python')

import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BOARD)
GPIO.setwarnings(False)

try:
    import asyncio
except ImportError:
    # Trollius >= 0.3 was renamed
    import trollius as asyncio

from autobahn.asyncio import wamp, websocket

################################################################################

from vice.shortcuts import *

################################################################################

Relais = {
    'r1': { 'pin': 12, 'label': "Lampe 1", 'state': False },
    'r2': { 'pin': 16, 'label': "Lampe 2", 'state': False },
}

Mapping = {
    'humid': dict(unit='%',   field=None),
    'temp':  dict(unit='C',   field=None),
    'heat':  dict(unit='%',   field=None),

    'soil':  dict(unit='%',   field=None),
    'lumen': dict(unit='L',   field=None),
    'power': dict(unit='kWh', field=None),
}

################################################################################

class MyFrontendComponent(wamp.ApplicationSession):
    def onConnect(self):
        self.mifare = MFRC522()

        self.join(u"streams.scubadev.com", [u"wampcra"], u"joe")

    def onChallenge(self, challenge):
        if challenge.method == u"wampcra":
            signature = auth.compute_wcs(u"secret2".encode('utf8'),
                                      challenge.extra['challenge'].encode('utf8'))
            return signature.decode('ascii')
        else:
            raise Exception("don't know how to handle authmethod {}".format(challenge.method))

    @asyncio.coroutine
    def onJoin(self, details):
        self.tty = serial.Serial('/dev/ttyAMA0', 9600)

        # REGISTER a procedure for remote calling
        #
        def on_meta():
            return {
                'relais': Relais,
            }

        reg = yield self.register(on_meta, 'jarvis.phenix.meta')
        print("procedure meta() registered: {}".format(reg))

        def on_relai(key, state):
            if key in Relais:
                GPIO.output(Relais[key]['pin'], not state)

            print("relai() called with {} and {}".format(key, state))

            return state

        reg = yield self.register(on_relai, 'jarvis.phenix.relai')
        print("procedure relai() registered: {}".format(reg))

        while self.tty.isOpen():
            raw = self.tty.readline()

            if '#' in raw:
                comp, raw = raw.split('#', 1)

                payload = dict([
                    ln.split(':', 1)
                    for ln in lst.split('|')
                    if ':' in ln
                ])

                target = dict([
                    (Mapping[prop].get('field', None) or prop, {
                        'value': float(value),
                        'unit':  Mapping[prop]['unit'],
                    })
                    for prop,value in payload.iteritems()
                    if prop in Mapping
                ])

                yield self.publish('jarvis.phenix.weather', [target])
                

        self.tty.close()

    ############################################################################

    def onLeave(self, details):
        self.disconnect()

    def onDisconnect(self):
        asyncio.get_event_loop().stop()

################################################################################

if __name__ == '__main__':
    print "Preparing the agent ..."
    session_factory = wamp.ApplicationSessionFactory()
    session_factory.session = MyFrontendComponent

    transport_factory = websocket.WampWebSocketClientFactory(session_factory,
                                                             debug=True,
                                                             debug_wamp=True)

    loop = asyncio.get_event_loop()
    coro = loop.create_connection(transport_factory, '10.7.0.1:9000')
    loop.run_until_complete(coro)

    loop.run_forever()

    GPIO.cleanup()
