#!/usr/bin/env python
#-*- coding: utf-8 -*-

import datetime

from twisted.internet.defer import inlineCallbacks

from autobahn import wamp
from autobahn.twisted.util import sleep
from autobahn.twisted.wamp import ApplicationSession
from autobahn.twisted.wamp import ApplicationSession

################################################################################

#import RPi.GPIO as GPIO

#from vice.contrib.AdaFrut.RFID import MFRC522

################################################################################

class Component(ApplicationSession):
    """
    A simple time service application component.
    """
    @inlineCallbacks
    def onJoin(self, details):
        print("session attached")

        if False:
            # subscribe all methods on this object decorated with "@wamp.subscribe"
            # as PubSub event handlers
            ##
            results = yield self.subscribe(self)

            for success, res in results:
                if success:
                    # res is an Subscription instance
                    print("Ok, subscribed handler with subscription ID {}".format(res.id))
                else:
                    # res is an Failure instance
                    print("Failed to subscribe handler: {}".format(res.value))
        else:
            state, result = yield self.subscribe(self.pubsub, 'jarvis.phenix.relai')

            if state:
                # res is an Subscription instance
                print("Ok, subscribed handler with subscription ID {}".format(result.id))
            else:
                # res is an Failure instance
                print("Failed to subscribe handler: {}".format(result.value))

        self.doLoop()

    #***************************************************************************

    #def onDisconnect(self):
    #    print("disconnected")

    #    reactor.stop()

    ############################################################################

    def doLoop(self):
        if hasattr(self, 'loop'):
            if callable(self.loop):
                try:
                    self.loop()
                except Exception,ex:
                    print ex

                self.factory.loop.call_later(1, self.doLoop)

    ############################################################################

    def loop(self):
        try:
            import Adafruit_DHT

            h, t = Adafruit_DHT.read_retry(Adafruit_DHT.DHT11, 4)
        except Exception as ex:
            pass

        if h is None:
            h = randrange(0, 100, 0.1, float)

        if t is None:
            t = randrange(20, 40, 0.01, float)

        p = randrange(50, 1000, 1, int)

        yield self.publish('jarvis.phenix.weather', [{
            'temp': {
                'value': t,
                'unit': 'C',
            },
            'humid': {
                'value': h,
                'unit': '%',
            },
            'power': {
                'value': p,
                'unit': 'kWh',
            },
        }])

    ############################################################################

    #@wamp.subscribe('jarvis.phenix.relai')
    def pubsub_relai(self, key, state):
        print("relai() called with {} and {}".format(key, state))

        return state

if __name__ == '__main__':
    from autobahn.twisted.wamp import ApplicationRunner

    runner = ApplicationRunner("ws://streams.scubadev.com:9000", "streams.scubadev.com", debug=False)

    runner.run(Component)
