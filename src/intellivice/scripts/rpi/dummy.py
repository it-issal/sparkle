#!/usr/bin/env python
#-*- coding: utf-8 -*-

import sys ; sys.path.append('/vice/lib/python')

from vice.shortcuts import *

from vice.streams.interfaces.Jarvis import *

################################################################################

class DummyProtocol(JarvisProtocol):
    def setup(self):
        pass

    def loop(self):
        self.package('dummy', {
            'temp': {
                'value': randrange(20, 40, 0.01, float),
                'unit': 'C',
            },
            'humid': {
                'value': randrange(0, 100, 0.1, float),
                'unit': '%',
            },
        })

################################################################################

if __name__ == '__main__':
    run_vice_protocol(DummyProtocol,
        endpoint = ('streams.scubadev.com', 9000),
        realm    = "ws://streams.scubadev.com:9000",
        debug    = True,
    )
