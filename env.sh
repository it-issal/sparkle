#!/bin/sh

if [[ $SHL_OS == "windows" ]] ; then
    export CYG_PKGs=$CYG_PKGs",octave,R"
fi

#if [[ $SHL_OS == "macosx" ]] ; then
#	ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)"
#fi

if [[ $SHL_OS == "ubuntu" ]] ; then
    export APT_PKGs="$APT_PKGs wvdial hostapd"
    export APT_PKGs="$APT_PKGs radvd quagga avahi-daemon"
    export APT_PKGs="$APT_PKGs ntp isc-dhcp-server bind9 squid3"
    export APT_PKGs="$APT_PKGs duplicity collectd collectd-utils"
    export APT_PKGs="$APT_PKGs openvpn ipsec xl2tpd"
    export APT_PKGs="$APT_PKGs apt-cachier-ng apt-mirror" # apt-btrfs-snaphost
fi

if [[ $SHL_OS == "raspbian" ]] ; then
    export APT_PKGs="$APT_PKGs wvdial hostapd"
fi
