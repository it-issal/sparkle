FROM    os2use/ubuntu:14.04

#*******************************************************************************

# Install packages

RUN apt-get install --force-yes -qy python2.7-dev python-pip python-virtualenv cython nodejs ruby

RUN gem install --no-rdoc --no-ri foreman

#*******************************************************************************

COPY . /beacon

RUN mkdir -p /beacon/var

WORKDIR /beacon

RUN /beacon/boot/strap

#*******************************************************************************

VOLUME /beacon/var

CMD ["/beacon/boot/load"]
