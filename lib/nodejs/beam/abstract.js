var sys=require('sys'), util = require('util'), url=require('url'), S=require('string'), YAML = require('js-yaml');
var dns = require('native-dns');

/**************************************************************************************************************/

exports.Host = function (cfg) {
    this.cfg = cfg;
    
    this.owner   = cfg.owner;
    this.name   = cfg.name;
    
    this.hood    = cfg.name+'.hood.'+this.owner+'.zone';
    this.fog     = cfg.name+'.fog.'+this.owner+'.zone';
};

exports.Host.prototype.resolve4 = function () {
    return this.cfg.addr4;
};

exports.Host.prototype.resolve6 = function () {
    return this.cfg.addr6;
};

exports.Host.prototype.detect = function (context, route) {
    if (context.fqdn==this.name) {
        context.host   = this;
        context.family = 0;
        
        context.route = route;
    } else if (context.fqdn==this.hood) {
        context.host   = this;
        context.family = 4;
        
        context.route = route;
    } else if (context.fqdn==this.fog) {
        context.host   = this;
        context.family = 6;
        
        context.route = route;
    }
    
    return context;
};

exports.Host.prototype.process_dns = function (context) {
    console.log("\t-> Resolving <host>[ ",this.name," ]{ ",this.owner," }");
    
    var targets = this.resolve4();
    
    for (i=0 ; i<targets.length ; i++) {
        context.response.answer.push(dns.A({
            name: this.hood,
            address: targets[i],
            ttl: 600,
        }));
    }
    
    var targets = this.resolve6();
    
    for (i=0 ; i<targets.length ; i++) {
        context.response.additional.push(dns.AAAA({
            name: this.fog,
            address: targets[i],
            ttl: 600,
        }));
    }
};

/**************************************************************************************************************/

exports.Backend = function (cfg) {
    this.cfg = cfg;
    
    this.owner   = cfg.owner;
    this.name   = cfg.name;
    this.fqdn    = cfg.name+'.hive.'+this.owner+'.zone';
    
    this.aliases = cfg.aliases;
};

exports.Backend.prototype.detect = function (context, route) {
    if (context.fqdn==this.fqdn) {
        context.backend = this;
        
        context.route = route;
    } else {
        /*
        for (var i=0 ; i<this.aliases.length ; i++) {
            if (this.aliases[i]==context.fqdn) {
                context.backend = this;
                
                context.route = route;
            }
        }
        //*/
    }
    
    return context;
};

exports.Backend.prototype.process_dns = function (context) {
    console.log("\t-> Resolving <hive>[ ",this.name," ]{ ",this.owner," }");
    
    context.response.answer.push(dns.CNAME({
        name: context.fqdn,
        data: this.fqdn,
        ttl: 600,
    }));
    
    for (i=0 ; i<this.targets.length ; i++) {
        context.response.answer.push(dns.CNAME({
            name: this.name+'.hive.'+this.owner+'.zone',
            data: this.targets[i],
            ttl: 600,
        }));
    }
};

/**************************************************************************************************************/

exports.Application = function (cfg) {
    this.cfg = cfg;
    
    this.owner   = cfg.owner;
    this.name   = cfg.name;
    this.fqdn    = cfg.name+'.vhost.'+this.owner+'.zone';
    
    this.domains = cfg.domains;
    this.aliases = cfg.aliases;
    
    this.gateway = cfg.gateway;
};

exports.Application.prototype.detect = function (context, route) {
    if (context.fqdn==this.name+'.vhost.'+this.owner+'.zone') {
        context.vhost = this;
        
        context.route = route;
    } else {
        for (var i=0 ; i<this.aliases.length && context.route==null ; i++) {
            if (this.aliases[i]==context.fqdn) {
                context.vhost = this;
                
                context.route = route;
            }
        }
        
        for (var i=0 ; i<this.domains.length && context.route==null ; i++) {
            if ((context.fqdn==this.domains[i]) || S(context.fqdn).endsWith('.'+this.domains[i])) {
                context.vhost = this;
                
                context.route = route;
            }
        }
    }
    
    return context;
};

exports.Application.prototype.process_dns = function (context) {
    console.log("\t-> Resolving <vhost>[ ",context.vhost.name," ]{ ",context.vhost.owner," }");
    
    context.response.answer.push(dns.CNAME({
        name: context.fqdn,
        data: context.vhost.fqdn,
        ttl: 600,
    }));
    
    switch (context.vhost.gateway.type) {
        case 'host':
            for (i=0 ; i<exports.meta['hosts'].length ; i++) {
                if (context.vhost.gateway.narrow.owner==exports.meta['hosts'][i].owner) {
                    if (context.vhost.gateway.narrow.name==exports.meta['hosts'][i].name) {
                        exports.meta['hosts'][i].process_dns(context);
                    }
                }
            }
            break;
        
        default:
            context.response.answer.push(dns.A({
                name: context.fqdn,
                address: '127.0.0.1',
                ttl: 600,
            }));
            context.response.additional.push(dns.AAAA({
                name: context.fqdn,
                address: '::1',
                ttl: 600,
            }));
            break;
    }
};

