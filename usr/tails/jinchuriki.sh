#!/bin/sh

export APT_PKGs="$APT_PKGs havegd"

alias JINCHURIKI_HOMELAND=$RONIN_CHARM_DIR/jinchuriki

if [[ $JINCHURIKI_TAILS == "" ]] ; then
	alias JINCHURIKI_TAILS="beacon"
fi

#*******************************************************************************

if [[ $ZSH != '' ]] ; then
	ZSH_PLUGINs+=(supervisor)
	ZSH_PLUGINs+=(docker vagrant)
	#ZSH_PLUGINs+=(cap)
	#ZSH_PLUGINs+=(tugboat knife knife_ssh)
fi

#*******************************************************************************

for tail in $JINCHURIKI_TAILS ; do
	TARGET_DIR=$JINCHURIKI_HOMELAND/tails/$key

	if [[ -e $TARGET_DIR ]] ; then
		for folder in opt var ; do
	    if [[ ! -d $TARGET_DIR/$folder ]] ; then
				mkdir -p $TARGET_DIR/$folder
			fi
	  done

		if [[ -f $TARGET_DIR/env.sh ]] ; then
			source $TARGET_DIR/env.sh
		fi
	fi
done
