#!/bin/sh

if [[ $JINCHURIKI_PATHs=="" ]] ; then
	export JINCHURIKI_PATHs="ipv6"
fi

for JINCHURIKI_PATH in $JINCHURIKI_PATHs ; do
  case "$JINCHURIKI_PATH" in
    alter)
      export APT_PKGs=$APT_PKGs" tor i2p"
      ;;
    ipv6)
      export APT_PKGs=$APT_PKGs" miredo radvd"
      ;;
    vpn)
      export APT_PKGs=$APT_PKGs" ipsec xl2tp"
      export APT_PKGs=$APT_PKGs" openvpn"
      ;;
  esac
done

#*****************************************************************************************************

# eg: ufw_allow_network 192.168.1 24 port 22 proto tcp

ufw_allow_network () {
    ufw allow in from $1.0/$2 to $1.1 $3
}
