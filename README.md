# Shell commands #

* dehliz
* shield
* uplink

# Features #

* Linux gateway with firewall / routing capablities in IPv4 / IPv6 & transparent proxying.
* Basic daemons with Caching abalities & Proxying features.

* Configure fileshares across Homeless hosts.
* Configure fileshares across multiple platforms.

* Basic defense daemons with evasive abalities.
* Decentralized anti-DDoS barrier.
* 2-way learning barrier.
* Management & annonce of REST-able resources cache/env with NoSQL backend.

# Docker Images #

### [Defender](./srv/defender/) ###

Network Shield

```
docker run -v /srv:/srv -p 21:21 -d maherops/beacon:defender
```

### [Dehliz](./srv/dehliz/) ###

File-Server-as-a-Service

```
docker run -v /srv:/srv -p 21:21 -d maherops/beacon:dehliz
```

### [PenTest](./srv/pentest/) ###

Penetration Testing

```
docker run -v /srv:/srv -p 21:21 -it maherops/beacon:pentest -- bash
```

### [Proxy](./srv/proxy/) ###

Transparent Proxy

```
docker run -v /var/cache/apt-cacher-ng:/var/cache/apt-cacher-ng -p 3124:3124 -p 3142:3142 -d maherops/beacon:proxy
```

### [Vault](./srv/vault/) ###

Virtual File Stroage

```
docker run -v /srv:/srv -p 21:80 -d maherops/beacon:vault
```
