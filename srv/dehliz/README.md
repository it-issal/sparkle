# Shell commands #

* dehliz

# Features #

* Configure fileshares across Homeless hosts.
* Configure fileshares across multiple platforms.

# List of packages #

### File Analysis : ###

* clamav
* chkrootkit
* rkhunter

### File Server : ###

* vsFTPd

* NFS Server

* Net-A-Talk
* Samba4
