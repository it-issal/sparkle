# Shell commands #

* uplink

# Features #

* Linux gateway with firewall / routing capablities in IPv4 / IPv6 & transparent proxying.
* Basic daemons with Caching abalities & Proxying features.

* Management & annonce of REST-able resources cache/env with NoSQL backend.

# List of packages #

### Log Analysis : ###

* fail2ban

### Traffic Analysis : ###

* psad

### Networking : ###

* quagga
* ufw

* isc-dhcp-server

* hostapd
* wvdial

### Tunneling & VPN : ###

* ipsec
* openvpn

### Cache / Proxy : ###

* apt-cacher-ng
